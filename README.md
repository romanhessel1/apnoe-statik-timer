![](resources/drawables/Apnoe-Statik_240.png)

# Apnoe Statik Timer
Ein Programm für Garmin Uhren. Der "Apnoe-Statik-Timer" ist ein einfacher Timer und eine Stoppuhr zum Trainieren von Apnoe-Statik. Es ist geschrieben in "Monkey C", einer Sprache die speziell für Garmin Uhren entwickelt wurde.

# Apnea Static Timer
A program for Garmin watches. The “Apnea-Static-Timer" is a simple timer and stopwatch for training apnea statics. It is written in “Monkey C" a language specially developed for Garmin watches.

